-- migrate:up
CREATE TYPE case_status_type AS ENUM (
    'Disposed',
    'Dismissed',
    'Active',
    'Appeal',
    'Bankruptcy'
);
CREATE TYPE judgment_type AS ENUM (
    'Agreed Judgment',
    'Default Judgment',
    'Final Judgment',
    'Granted'
);
CREATE TYPE disposition_type AS ENUM (
    'Agreed Judgment (OCA)',
    'All Other Dispositions (OCA)',
    'All Other Dispositions Except Dismissal (OCA)',
    'Appeal Filed',
    'Default Judgment (OCA)',
    'Dismissed for Want of Prosecution (OCA)',
    'Judgment Set Aside',
    'Non-suited or Dismissed by Plaintiff (OCA)',
    'Transfer Case (OCA)',
    'Trial by Jury (OCA)',
    'Trial or Hearing by Judge (OCA)'
);
-- CREATE TYPE nature_of_claim AS ENUM ('Small Claims', "Occupational Driver's License", 'Handgun License', 'Eviction', "Driver's License Suspension Hearing", 'Debt Claim', NULL);

CREATE TABLE
    cases (
        uuid UUID DEFAULT UUID_GENERATE_V4() PRIMARY KEY,
        court_uuid UUID NOT NULL,
        location_uuid UUID,
        FOREIGN KEY (court_uuid) REFERENCES courts (uuid),
        FOREIGN KEY (location_uuid) REFERENCES locations (uuid),
        case_number VARCHAR(255) NOT NULL,
        file_date DATE NOT NULL,
        case_name TEXT,
        defendant_name TEXT,
        plaintiff_name TEXT,
        case_status case_status_type,
        judgment_text judgment_type,
        disposition_date DATE,
        disposition_desc disposition_type,
        nature_of_claim VARCHAR(255),
        -- claim_amount ,
        -- judgment_amount ,
        -- judgment_in_favor_of ,
        -- judgment_against ,
        UNIQUE(case_number)
    );


-- migrate:down
DROP TABLE cases;
DROP TYPE disposition_type;
DROP TYPE judgment_type;
DROP TYPE case_status_type;