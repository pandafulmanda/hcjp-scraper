-- migrate:up
ALTER TABLE locations
ALTER COLUMN lat_lng TYPE GEOMETRY;

-- migrate:down
ALTER TABLE locations
ALTER COLUMN lat_lng TYPE GEOGRAPHY;
