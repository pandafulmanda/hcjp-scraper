-- migrate:up
CREATE TABLE
    counties (
        uuid UUID DEFAULT UUID_GENERATE_V4() PRIMARY KEY,
        source_url VARCHAR(255) NOT NULL
    );

CREATE TABLE
    courts (
        uuid UUID DEFAULT UUID_GENERATE_V4() PRIMARY KEY,
        source_id VARCHAR(255) NOT NULL,
        name VARCHAR(255) NOT NULL,

        county_uuid UUID,
        FOREIGN KEY (county_uuid) REFERENCES counties (uuid)
    );

-- migrate:down
DROP TABLE courts;
DROP TABLE counties;
