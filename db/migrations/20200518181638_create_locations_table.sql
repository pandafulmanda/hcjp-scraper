-- migrate:up
CREATE TABLE
    places (
        place_id VARCHAR(255) NOT NULL,
        data JSONB,
        source_url  VARCHAR(255),
        created_at TIMESTAMP DEFAULT NOW(),
        UNIQUE(place_id)
    );

CREATE TABLE
    locations (
        uuid UUID DEFAULT UUID_GENERATE_V4() PRIMARY KEY,
        address_alias VARCHAR(255),
        address_line_1 VARCHAR(255),
        city VARCHAR(255),
        "state" VARCHAR(255),
        zip VARCHAR(255),
        lat_lng GEOGRAPHY,
        property_type VARCHAR(255),
        property_name VARCHAR(255),

        place_id VARCHAR(255) NOT NULL,
        FOREIGN KEY (place_id) REFERENCES places (place_id),
        UNIQUE(address_alias)
    );

-- migrate:down
DROP TABLE locations;
DROP TABLE places;
