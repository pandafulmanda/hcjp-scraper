-- migrate:up
CREATE TABLE extracts (
    uuid UUID DEFAULT UUID_GENERATE_V4() PRIMARY KEY,

    case_number VARCHAR(255) NOT NULL,
    court_uuid UUID NOT NULL,

    request_type VARCHAR(255) NOT NULL,

    FOREIGN KEY (case_number) REFERENCES cases (case_number),
    FOREIGN KEY (court_uuid) REFERENCES courts (uuid),

    -- file_date date NOT NULL,
    -- case_name TEXT,
    -- defendant_name TEXT,
    -- plaintiff_name TEXT,
    -- case_status case_status_type,
    -- judgment_text judgment_type,
    -- disposition_date date,
    -- disposition_desc disposition_type,
    -- nature_of_claim VARCHAR(255),

    order_in_file INT,
    claim_amount MONEY,
    next_hearing_desc TEXT,
    next_hearing_date DATE,
    next_hearing_time TIME,
    judgment_date DATE,
    judgment_in_favor_of TEXT,
    judgment_against TEXT,
    judgment_amount MONEY,
    attorney_fees MONEY,
    court_costs MONEY,
    pre_judg_int_rate DECIMAL(5,2),
    post_judg_int_rate DECIMAL(5,2),
    source_path VARCHAR(255)
);

-- migrate:down

DROP TABLE extracts;