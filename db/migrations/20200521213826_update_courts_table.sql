-- migrate:up
ALTER TABLE courts
DROP COLUMN name;

ALTER TABLE courts
ADD COLUMN precinct INT NOT NULL,
ADD COLUMN place INT NOT NULL;


-- migrate:down
ALTER TABLE courts
ADD COLUMN name VARCHAR
(255) NOT NULL,
DROP COLUMN precinct,
DROP COLUMN place;