-- migrate:up
ALTER TABLE counties
ADD COLUMN name character varying
(255) NOT NULL;

INSERT INTO counties
    (name, source_url)
VALUES
    ('Harris', 'https://jpwebsite.harriscountytx.gov/PublicExtracts/search.jsp');

-- migrate:down
ALTER TABLE counties
DROP COLUMN name;

DELETE FROM counties 
WHERE source_url = 'https://jpwebsite.harriscountytx.gov/PublicExtracts/search.jsp';