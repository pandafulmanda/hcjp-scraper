-- migrate:up
CREATE TYPE member_type
    AS ENUM (
        'plaintiff', 'defendant',
        'judge', 'attorney_plaintiff', 'attorney_defendant',
        'plaintiff_other', 'defendant_other'
    );

CREATE TABLE
    members (
        uuid UUID DEFAULT UUID_GENERATE_V4() PRIMARY KEY,
        "name" VARCHAR(255) NOT NULL,
        UNIQUE("name")
    );

CREATE TABLE
    case_members (
        uuid UUID DEFAULT UUID_GENERATE_V4() PRIMARY KEY,
        case_number VARCHAR(255) NOT NULL,
        member_uuid uuid NOT NULL,
        representing_uuid uuid,
        location_uuid uuid,
        FOREIGN KEY (case_number) REFERENCES cases (case_number),
        FOREIGN KEY (member_uuid) REFERENCES members (uuid),
        FOREIGN KEY (representing_uuid) REFERENCES members (uuid),
        FOREIGN KEY (location_uuid) REFERENCES locations (uuid),
        address_line_2 VARCHAR(255),
        relationship member_type NOT NULL
    );

-- migrate:down
DROP TABLE case_members;
DROP TABLE members;
DROP TYPE member_type;