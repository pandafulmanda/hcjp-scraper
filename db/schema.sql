SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION tablefunc IS 'functions that manipulate whole tables, including crosstab';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: case_status_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.case_status_type AS ENUM (
    'Disposed',
    'Dismissed',
    'Active',
    'Appeal',
    'Bankruptcy'
);


--
-- Name: disposition_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.disposition_type AS ENUM (
    'Agreed Judgment (OCA)',
    'All Other Dispositions (OCA)',
    'All Other Dispositions Except Dismissal (OCA)',
    'Appeal Filed',
    'Default Judgment (OCA)',
    'Dismissed for Want of Prosecution (OCA)',
    'Judgment Set Aside',
    'Non-suited or Dismissed by Plaintiff (OCA)',
    'Transfer Case (OCA)',
    'Trial by Jury (OCA)',
    'Trial or Hearing by Judge (OCA)'
);


--
-- Name: judgment_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.judgment_type AS ENUM (
    'Agreed Judgment',
    'Default Judgment',
    'Final Judgment',
    'Granted'
);


--
-- Name: member_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.member_type AS ENUM (
    'plaintiff',
    'defendant',
    'judge',
    'attorney_plaintiff',
    'attorney_defendant',
    'plaintiff_other',
    'defendant_other'
);


--
-- Name: on_update_timestamp(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.on_update_timestamp() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    NEW.updated_at = now();
    RETURN NEW;
  END;
$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: case_members; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_members (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    case_number character varying(255) NOT NULL,
    member_uuid uuid NOT NULL,
    representing_uuid uuid,
    location_uuid uuid,
    address_line_2 character varying(255),
    relationship public.member_type NOT NULL
);


--
-- Name: cases; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cases (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    court_uuid uuid NOT NULL,
    location_uuid uuid,
    case_number character varying(255) NOT NULL,
    file_date date NOT NULL,
    case_name text,
    defendant_name text,
    plaintiff_name text,
    case_status public.case_status_type,
    judgment_text public.judgment_type,
    disposition_date date,
    disposition_desc public.disposition_type,
    nature_of_claim character varying(255)
);


--
-- Name: counties; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.counties (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    source_url character varying(255) NOT NULL
);


--
-- Name: courts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.courts (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    source_id character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    county_uuid uuid
);


--
-- Name: extracts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.extracts (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    case_number character varying(255) NOT NULL,
    court_uuid uuid NOT NULL,
    request_type character varying(255) NOT NULL,
    order_in_file integer,
    claim_amount money,
    next_hearing_desc text,
    next_hearing_date date,
    next_hearing_time time without time zone,
    judgment_date date,
    judgment_in_favor_of text,
    judgment_against text,
    judgment_amount money,
    attorney_fees money,
    court_costs money,
    pre_judg_int_rate numeric(5,2),
    post_judg_int_rate numeric(5,2),
    source_path character varying(255)
);


--
-- Name: locations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.locations (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    address_alias character varying(255),
    address_line_1 character varying(255),
    city character varying(255),
    state character varying(255),
    zip character varying(255),
    lat_lng public.geography,
    property_type character varying(255),
    property_name character varying(255),
    place_id character varying(255) NOT NULL
);


--
-- Name: members; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.members (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL
);


--
-- Name: places; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.places (
    place_id character varying(255) NOT NULL,
    data jsonb,
    source_url character varying(255),
    created_at timestamp without time zone DEFAULT now()
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: case_members case_members_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_members
    ADD CONSTRAINT case_members_pkey PRIMARY KEY (uuid);


--
-- Name: cases cases_case_number_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cases
    ADD CONSTRAINT cases_case_number_key UNIQUE (case_number);


--
-- Name: cases cases_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cases
    ADD CONSTRAINT cases_pkey PRIMARY KEY (uuid);


--
-- Name: counties counties_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.counties
    ADD CONSTRAINT counties_pkey PRIMARY KEY (uuid);


--
-- Name: courts courts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.courts
    ADD CONSTRAINT courts_pkey PRIMARY KEY (uuid);


--
-- Name: extracts extracts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.extracts
    ADD CONSTRAINT extracts_pkey PRIMARY KEY (uuid);


--
-- Name: locations locations_address_alias_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_address_alias_key UNIQUE (address_alias);


--
-- Name: locations locations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (uuid);


--
-- Name: members members_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.members
    ADD CONSTRAINT members_name_key UNIQUE (name);


--
-- Name: members members_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.members
    ADD CONSTRAINT members_pkey PRIMARY KEY (uuid);


--
-- Name: places places_place_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.places
    ADD CONSTRAINT places_place_id_key UNIQUE (place_id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: case_members case_members_case_number_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_members
    ADD CONSTRAINT case_members_case_number_fkey FOREIGN KEY (case_number) REFERENCES public.cases(case_number);


--
-- Name: case_members case_members_location_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_members
    ADD CONSTRAINT case_members_location_uuid_fkey FOREIGN KEY (location_uuid) REFERENCES public.locations(uuid);


--
-- Name: case_members case_members_member_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_members
    ADD CONSTRAINT case_members_member_uuid_fkey FOREIGN KEY (member_uuid) REFERENCES public.members(uuid);


--
-- Name: case_members case_members_representing_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_members
    ADD CONSTRAINT case_members_representing_uuid_fkey FOREIGN KEY (representing_uuid) REFERENCES public.members(uuid);


--
-- Name: cases cases_court_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cases
    ADD CONSTRAINT cases_court_uuid_fkey FOREIGN KEY (court_uuid) REFERENCES public.courts(uuid);


--
-- Name: cases cases_location_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cases
    ADD CONSTRAINT cases_location_uuid_fkey FOREIGN KEY (location_uuid) REFERENCES public.locations(uuid);


--
-- Name: courts courts_county_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.courts
    ADD CONSTRAINT courts_county_uuid_fkey FOREIGN KEY (county_uuid) REFERENCES public.counties(uuid);


--
-- Name: extracts extracts_case_number_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.extracts
    ADD CONSTRAINT extracts_case_number_fkey FOREIGN KEY (case_number) REFERENCES public.cases(case_number);


--
-- Name: extracts extracts_court_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.extracts
    ADD CONSTRAINT extracts_court_uuid_fkey FOREIGN KEY (court_uuid) REFERENCES public.courts(uuid);


--
-- Name: locations locations_place_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_place_id_fkey FOREIGN KEY (place_id) REFERENCES public.places(place_id);


--
-- PostgreSQL database dump complete
--


--
-- Dbmate schema migrations
--

INSERT INTO public.schema_migrations (version) VALUES
    ('20200515180341'),
    ('20200515180500'),
    ('20200518181638'),
    ('20200518182546'),
    ('20200518183646'),
    ('20200519165900');
