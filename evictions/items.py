# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class EvictionsItem(scrapy.Item):
    # define the fields for your item here like:
    file_urls = scrapy.Field()
    files = scrapy.Field()
    case_type = scrapy.Field()
    from_date = scrapy.Field()
    request_type = scrapy.Field()
    court = scrapy.Field()
    precinct = scrapy.Field()
    place = scrapy.Field()
    judge = scrapy.Field()
    response = scrapy.Field()
    request = scrapy.Field()


class DocketsItem(scrapy.Item):
    court = scrapy.Field()
    docket_link = scrapy.Field()
    docket_date = scrapy.Field()
    docket_time = scrapy.Field()
    docket_cases = scrapy.Field()
