from .list_helpers import create_chunks

HEADINGS = [
    'case_number',
    'case_type',
    'case_subtype',
    'case_file_date',
    'style_of_case',
    'nature_of_claim',
    'claim_amount',
    'case_status',

    'plaintiff_name',
    'plaintiff_address_line_1',
    'plaintiff_address_line_2',
    'plaintiff_address_city',
    'plaintiff_address_state',
    'plaintiff_address_zip',

    'plaintiff_attorney_name',
    'plaintiff_attorney_address_line_1',
    'plaintiff_attorney_address_line_2',
    'plaintiff_attorney_address_city',
    'plaintiff_attorney_address_state',
    'plaintiff_attorney_address_zip',

    'defendant_name',
    'defendant_address_line_1',
    'defendant_address_line_2',
    'defendant_address_city',
    'defendant_address_state',
    'defendant_address_zip',

    'defendant_attorney_name',
    'defendant_attorney_address_line_1',
    'defendant_attorney_address_line_2',
    'defendant_attorney_address_city',
    'defendant_attorney_address_state',
    'defendant_attorney_address_zip',

    'second_plaintiff_name',
    'second_plaintiff_address_line_1',
    'second_plaintiff_address_line_2',
    'second_plaintiff_address_city',
    'second_plaintiff_address_state',
    'second_plaintiff_address_zip',

    'second_plaintiff_attorney_name',
    'second_plaintiff_attorney_address_line_1',
    'second_plaintiff_attorney_address_line_2',
    'second_plaintiff_attorney_address_city',
    'second_plaintiff_attorney_address_state',
    'second_plaintiff_attorney_address_zip',

    'second_defendant_name',
    'second_defendant_address_line_1',
    'second_defendant_address_line_2',
    'second_defendant_address_city',
    'second_defendant_address_state',
    'second_defendant_address_zip',

    'second_defendant_attorney_name',
    'second_defendant_attorney_address_line_1',
    'second_defendant_attorney_address_line_2',
    'second_defendant_attorney_address_city',
    'second_defendant_attorney_address_state',
    'second_defendant_attorney_address_zip',

    'next_hearing_description',
    'next_hearing_date',
    'next_hearing_time',

    'disposition_description',
    'disposition_date',

    'judgment_text',
    'judgment_date',

    'judgment_in_favor_of',
    'judgment_against',
    'judgment_amount',

    'attorney_fees',
    'court_costs',
    'pre_judgment_interest_rate',
    'post_judgment_interest_rate',
]

HEADINGS_WITH_INDEX = dict(zip(HEADINGS, list(range(0, len(HEADINGS)))))

COLUMNS_WITH_ADDRESS = list(create_chunks([
    HEADINGS_WITH_INDEX[heading]
    for heading in HEADINGS
    if
        'address' in heading
], 5))

COLUMNS_WITH_MONEY = [
    HEADINGS_WITH_INDEX[heading]
    for heading in HEADINGS
    if
        'amount' in heading or
        'costs' in heading or
        'fees' in heading
]

COLUMNS_WITH_RATE = [
    HEADINGS_WITH_INDEX[heading]
    for heading in HEADINGS
    if
        'rate' in heading
]

COLUMNS_WITH_DATE = [
    HEADINGS_WITH_INDEX[heading]
    for heading in HEADINGS
    if
        'date' in heading
]

COLUMNS_WITH_TIME = [
    HEADINGS_WITH_INDEX[heading]
    for heading in HEADINGS
    if
        'time' in heading
]

COLUMNS_WITH_MEMBER = list(create_chunks([
    HEADINGS_WITH_INDEX[heading]
    for heading in HEADINGS
    if
        'plaintiff' in heading or
        'defendant' in heading
], 6))

HEADINGS_FOR_ADDRESS = [
    'line_1',
    'line_2',
    'city',
    'state',
    'zip',
]