COURTS = {
    "305": {"precinct": 1, "place": 1, "judge": "Carter"},
    "310": {"precinct": 1, "place": 2, "judge": "Patronella"},
    "315": {"precinct": 2, "place": 1, "judge": "Delgado"},
    "320": {"precinct": 2, "place": 2, "judge": "Risner"},
    "325": {"precinct": 3, "place": 1, "judge": "Stephens"},
    "330": {"precinct": 3, "place": 2, "judge": "Bates"},
    "335": {"precinct": 4, "place": 1, "judge": "Goodwin"},
    "340": {"precinct": 4, "place": 2, "judge": "Korduba"},
    "345": {"precinct": 5, "place": 1, "judge": "Ridgway"},
    "350": {"precinct": 5, "place": 2, "judge": "Williams"},
    "355": {"precinct": 6, "place": 1, "judge": "Vara"},
    "360": {"precinct": 6, "place": 2, "judge": "Rodriguez"},
    "365": {"precinct": 7, "place": 1, "judge": "Brown"},
    "370": {"precinct": 7, "place": 2, "judge": "Burney"},
    "375": {"precinct": 8, "place": 1, "judge": "Williamson"},
    "380": {"precinct": 8, "place": 2, "judge": "Ditta"}
}
