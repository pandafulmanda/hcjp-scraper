from datetime import datetime, timedelta, timezone

import re
from evictions.utils.list_helpers import create_chunks
from statistics import mode, StatisticsError

from scrapy.exceptions import NotConfigured

from airtable import Airtable

COURTS = {
    '11': {
        'airtable_id': 'recIlcYuGe9IXmfQE',
        'name': 'Precinct 1 Place 1'
    },
    '12': {
        'airtable_id': 'recESb25JkokIYvau',
        'name': 'Precinct 1 Place 2'
    },
    '21': {
        'airtable_id': 'recphYm16jpqrWwaR',
        'name': 'Precinct 2 Place 1'
    },
    '22': {
        'airtable_id': 'rec7m5MoDCByLv25f',
        'name': 'Precinct 2 Place 2'
    },
    '31': {
        'airtable_id': 'recfLRzZhgSSgEF0P',
        'name': 'Precinct 3 Place 1'
    },
    '32': {
        'airtable_id': 'rec9eGWdJnP1gr9vF',
        'name': 'Precinct 3 Place 2'
    },
    '41': {
        'airtable_id': 'recYxWNiMqB2VoPPl',
        'name': 'Precinct 4 Place 1'
    },
    '42': {
        'airtable_id': 'rechzoKtQrKgXYiZY',
        'name': 'Precinct 4 Place 2'
    },
    '51': {
        'airtable_id': 'rectzRNXFo8mJWRKt',
        'name': 'Precinct 5 Place 1'
    },
    '52': {
        'airtable_id': 'recA3Qj2mphZ5QAYT',
        'name': 'Precinct 5 Place 2'
    },
    '61': {
        'airtable_id': 'recArTMhEl5xSs67T',
        'name': 'Precinct 6 Place 1'
    },
    '62': {
        'airtable_id': 'rec56JkKZUelPfFlk',
        'name': 'Precinct 6 Place 2'
    },
    '71': {
        'airtable_id': 'recGRnuahRElXHDyc',
        'name': 'Precinct 7 Place 1'
    },
    '72': {
        'airtable_id': 'rec90BHv2zNccO3Rx',
        'name': 'Precinct 7 Place 2'
    },
    '81': {
        'airtable_id': 'recxeDuEfESgBzsq0',
        'name': 'Precinct 8 Place 1'
    },
    '82': {
        'airtable_id': 'recgFB0lF0SXQ2CSa',
        'name': 'Precinct 8 Place 2'
    },
}

def get_datetime_from(date, time):
    return datetime.strptime(f'{date} {time}-0500', '%m/%d/%Y %I:%M%p%z')

def make_docket_record(docket, end_time = None, duration = timedelta(minutes=30)):
    start_time = get_datetime_from(docket.get('docket_date'), docket.get('docket_time'))

    if end_time is not None:
        end_time = get_datetime_from(docket.get('docket_date'), end_time)
    elif duration is not None:
        end_time = start_time + duration

    docket_link = f"https://jpwebsite.harriscountytx.gov/WebDockets/GetDocketDetail?date={docket.get('docket_date')}&court={docket.get('court')}&media=screen#{docket.get('docket_time').replace(':', '')}"
    docket_record = {
        'fields': {
            'Court': [COURTS[docket.get('court')].get('airtable_id')],
            'Link to Docket': docket_link,
            'Start Time': start_time.isoformat(),
            'Number of Cases': sum_eviction_docket(docket)
        }
    }

    if end_time is not None:
        docket_record['fields']['End Time'] = end_time.isoformat()

    return docket_record

def get_common_timedelta(dockets):
    docket_datetimes = [get_datetime_from(docket.get('docket_date'), docket.get('docket_time')) for docket in dockets]
    deltas = [(value - docket_datetimes[index - 1]) for index, value in enumerate(docket_datetimes) if index is not 0]

    try:
        return mode(deltas)
    except StatisticsError as exception:
        print(exception)
        return None

def is_eviction_docket(docket):
    selecting_case_type = 'Eviction Docket'
    return selecting_case_type in [case.get('type_name') for case in docket.get('docket_cases')]

def sum_eviction_docket(docket):
    selecting_case_types = ['Eviction Docket', 'Hearing']
    return sum([case.get('num_cases') for case in docket.get('docket_cases') if case.get('type_name') in selecting_case_types])

def standardize_timestr(time_str):
    if time_str is None:
        return time_str

    time_str = re.sub('Z$', '', time_str)  

    standardized_time = datetime.fromisoformat(time_str)
    if standardized_time.utcoffset() is not None:
        standardized_time = (standardized_time - standardized_time.utcoffset()).replace(tzinfo=None)

    return standardized_time

def make_docket_index(docket):
    time_str = docket.get('Start Time')
    docket_time = standardize_timestr(time_str)

    return f"{'-'.join(docket.get('Court'))}--{datetime.isoformat(docket_time)}"

def index_dockets_by_court_and_time(dockets):
    return {
        make_docket_index(docket): docket
        for docket in dockets
    }

def index_records_by_court_and_time(records):
    return {
        make_docket_index(record.get('fields')): record
        for record in records
    }

def get_dict_diff_left(left_dict, right_dict):
    # where the leftmost dictionary's values are used if they are different from the rightmost
    return {
        key : left_dict[key]
    for
        key, value in left_dict.items()
    if (
            ' Time' in key and
            standardize_timestr(value) != standardize_timestr(right_dict.get(key))
        ) or (
            ' Time' not in key and
            value != right_dict.get(key)
        )
    }

class DocketToAirtable(object):
    def __init__(self, airtable_base_id):
        self.base_id = airtable_base_id

    @classmethod
    def from_crawler(cls, crawler):
        api_settings = crawler.settings.getdict("API_SETTINGS")
        if not api_settings or not api_settings['airtable_base_id']:
            raise NotConfigured

        return cls(api_settings['airtable_base_id'])

    def open_spider(self, spider):
        self.dockets_airtable = Airtable(self.base_id, 'Dockets')

    def get_item_meta(self, item):
        court = item.get('court')
        from_date = item.get('from_date').isoformat()
        to_date = item.get('to_date').isoformat()

        return {
            'court': COURTS[court].get('name'),
            'from_date': from_date,
            'to_date': to_date
        }

    def get_existing_dockets(self, item):
        meta = self.get_item_meta(item)

        date_same_or_after = 'OR(IS_SAME({Start Time}, "' + meta.get('from_date') + '", "day"),IS_AFTER({Start Time}, "' + meta.get('from_date') + '"))'
        date_same_or_before = 'OR(IS_SAME({Start Time}, "' + meta.get('to_date') + '", "day"),IS_BEFORE({Start Time}, "' + meta.get('to_date') + '"))'
        is_in_item_court = 'FIND("' + meta.get('court') + '", {Court})'
        is_scheduled = '{Status}=BLANK()'

        formula = 'AND(' + date_same_or_after + ',' + date_same_or_before + ',' + is_in_item_court + ',' + is_scheduled + ')'
        return self.dockets_airtable.get_all(formula = formula)

    async def process_item(self, item, spider):
        dockets = item.get('dockets')
        duration = get_common_timedelta(dockets)
        scraped_dockets = [
            make_docket_record(
                docket,
                end_time=(
                    (index + 1) < len(dockets) and
                    is_eviction_docket(dockets[index + 1]) and
                    dockets[index + 1].get('docket_time')
                ) or None,
                duration=duration
            )
            for index, docket in enumerate(dockets)
            if is_eviction_docket(docket)
        ]

        existing_dockets = self.get_existing_dockets(item)

        scraped_dockets_indexed = index_records_by_court_and_time(scraped_dockets)
        existing_dockets_indexed = index_records_by_court_and_time(existing_dockets)

        scraped_dockets_keys = scraped_dockets_indexed.keys()
        existing_dockets_keys = existing_dockets_indexed.keys()

        dockets_to_create = [
            docket
        for (key, docket) in scraped_dockets_indexed.items() if key not in existing_dockets_keys]
        dockets_to_mark_for_confirmation = [{
            'id': docket.get('id'),
            'fields': {'Status': 'Needs Verification'}
        } for (key, docket) in existing_dockets_indexed.items() if key not in scraped_dockets_keys]
        dockets_updates = [{
            'id': docket.get('id'),
            'fields': get_dict_diff_left(
                scraped_dockets_indexed.get(key).get('fields'),
                docket.get('fields')
            )
        } for (key, docket) in existing_dockets_indexed.items() if key in scraped_dockets_keys]
        dockets_to_update = [docket for docket in dockets_updates if len(docket.get('fields')) > 0]

        dockets_with_updates = dockets_to_mark_for_confirmation + dockets_to_update

        if len(dockets_to_create) > 0:
            all_new_dockets = create_chunks(dockets_to_create, 10)
            airtable_post_requests = [self.dockets_airtable._post(
                self.dockets_airtable.url_table, json_data={'records': records}
            ) for records in all_new_dockets]

        if len(dockets_with_updates) > 0:
            all_update_dockets = create_chunks(dockets_with_updates, 10)
            airtable_patch_requests = [self.dockets_airtable._patch(
                self.dockets_airtable.url_table, json_data={'records': records}
            ) for records in all_update_dockets]

        return {
            'existing': existing_dockets_indexed,
            'scraped': scraped_dockets_indexed,
            'new': dockets_to_create,
            'updates': dockets_to_update,
            'unconfirmed': dockets_to_mark_for_confirmation
        }
    
    def close_spider(self, spider):
        pass