# -*- coding: utf-8 -*-
from .database import DatabasePipeline

import googlemaps
import usaddress

import csv
import re
from io import StringIO
from datetime import datetime, date, time, timezone
from sqlalchemy.sql import select

from evictions.settings import API_SETTINGS
from evictions.utils.headings import COLUMNS_WITH_ADDRESS, COLUMNS_WITH_MEMBER, COLUMNS_WITH_DATE, COLUMNS_WITH_TIME, COLUMNS_WITH_MONEY, COLUMNS_WITH_RATE, HEADINGS_FOR_ADDRESS
from evictions.pipelines.tables import locations_table, places_table

gmaps = googlemaps.Client(key=API_SETTINGS.get('gmaps_api_key'))

NORTH = 30.128310
EAST = -95.028755
SOUTH = 29.485913
WEST = -95.826341

def clean_value(value):

    if value is None:
        return None
    
    trimmed_value = value.strip()

    if trimmed_value is '' or trimmed_value == 'null':
        return None
    
    return trimmed_value

def get_data(item):
    item_file = StringIO(item.get('response').body.decode('ASCII'))
    data = csv.reader(item_file, delimiter="\t")
    next(data) # skip header
    next(data) # skip blank line -- these should be validated
    return data

def process_values(index, value):
    cleaned = clean_value(value)

    if cleaned is None:
        return cleaned

    if index in COLUMNS_WITH_DATE:
        return datetime.strptime(cleaned, '%m/%d/%Y')

    if index in COLUMNS_WITH_TIME:
        return datetime.strptime(cleaned, '%I:%M %p')

    if index in COLUMNS_WITH_MONEY:
        return float(cleaned)

    if index in COLUMNS_WITH_RATE:
        return float(cleaned)

    return cleaned

# TODO actually deal with this.
def is_value_address_part(value):
    skip = ['Home -', 'House', '(house)', 'House @', 'Home-', 'House;', 'House =', '(House)', 'house', 'A House @', 'House at -', 'House At', 'My house', 'House at -', '2 Bed House', 'House/Townhome']

    if value in skip:
        return False
    return True

def parse_for_address(address_values):
    cleaner = re.compile('(\$| room).*$')

    has_address = [value for value in address_values if value is not None] != []

    if has_address is False:
        return None, {}

    address = dict(zip(HEADINGS_FOR_ADDRESS, address_values))
    address_cleaned = {}
    for (key, value) in address.items():

        if value is not None and is_value_address_part(value):
            cleaned_value = cleaner.sub('', value).strip()
            address_cleaned[key] = cleaned_value
        else:
            address_cleaned[key] = ''
    address_combined = f'{address_cleaned.get("line_1")} {address_cleaned.get("line_2")}, {address_cleaned.get("city")}, {address_cleaned.get("state")} {address_cleaned.get("zip")}'

    parts_to_use = [
        'BuildingName',
        'AddressNumberPrefix', 'AddressNumber', 'AddressNumberSuffix',
        'StreetNamePreDirectional', 'StreetNamePreModifier', 'StreetNamePreType', 'StreetName', 'StreetNamePostDirectional', 'StreetNamePostModifier', 'StreetNamePostType',
        'PlaceName', 'StateName', 'ZipCode',
    ]

    try:
        address_parts, address_type = usaddress.tag(address_combined)

        if address_parts.get('AddressNumber') is None and address_parts.get('StreetName') is None:
            raise ValueError(f'Address not complete for parsing: {address_combined}')

        address_parts_searchable = [address_parts.get(part) for part in parts_to_use if address_parts.get(part) is not None]

        return address_parts_searchable, address_parts

    except Exception as exception:
        # TODO: actually log as error.
        print(exception, address_values)
        return None, {}

def find_place(address_search_string):
    return gmaps.find_place(address_search_string,
        input_type = 'textquery',
        location_bias = f'rectangle:{SOUTH},{WEST}|{NORTH},{EAST}',
        fields = ['business_status', 'formatted_address', 'geometry', 'name', 'permanently_closed', 'photos', 'place_id', 'plus_code', 'types']
    )

class CaseToDatabasePipeline(DatabasePipeline):
    async def process_item(self, item, spider):
        data = list(get_data(item))[0:2]
        for extract in data:
            await self.handle_extract(extract)
        return item

    async def find_place(self, address_alias):
        place_selector = select([locations_table]).where(locations_table.c.address_alias == address_alias)

        place_results = self.conn.execute(place_selector)
        place_result = place_results.fetchone()

        if place_result is None:
            places = find_place(address_alias)
            match = places.get('candidates')[0]

            geometry = match.get('geometry').get('location')
            address_line_1, city, stateZip, country =  match.get('formatted_address').split(', ')
            state, zipCode = stateZip.split(' ')

            location = {
                'address_alias': address_alias,
                'address_line_1': address_line_1,
                'city': city,
                'state': state,
                'zip': zipCode,
                'lat_lng': ''
            }

        return places

    async def handle_location(self, address_values):
        address_parts_searchable, address_parts = parse_for_address(address_values)

        if address_parts_searchable is None:
            return

        address_alias = " ".join(address_parts_searchable)

        places = await self.find_place(address_alias)

        address_line_2 = f'{address_parts.get("OccupancyType")} {address_parts.get("OccupancyIdentifier")}'

        print(places, address_parts)
        # self.conn.execute(items.insert(), item.get('items'))

    async def handle_member(self, member):
        address_values = member[1:]
        location = await self.handle_location(address_values)
        return location

    async def handle_extract(self, extract):
        values = [process_values(index, value) for (index, value) in enumerate(extract)]
        member_values = [values[member_indices[0]:(member_indices[-1] + 1)] for member_indices in COLUMNS_WITH_MEMBER]
        members = [await self.handle_member(member) for member in member_values]

        return { 'values': values, 'members': members }