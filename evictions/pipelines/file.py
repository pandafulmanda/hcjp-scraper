# -*- coding: utf-8 -*-
from scrapy.pipelines.files import FilesPipeline
import scrapy
from datetime import date

class MyFilesPipeline(FilesPipeline):

    def get_media_requests(self, item, info):
        for file_url in item['file_urls']:
            formatted_date = item['from_date'].strftime("%Y-%m")
            meta = {'date':  formatted_date,
                    'court': item['court'],
                    'case_type': item['case_type'],
                    'request_type': item['request_type']}
            yield scrapy.Request(url=file_url, meta=meta)

    def file_path(self, request, response=None, info=None):
        file_name = "evictions/{date}/{court}/{request_type}".format(
            request_type=request.meta.get('request_type'), date=request.meta.get('date'), court=request.meta.get('court'))
        return file_name
