from sqlalchemy import Table, MetaData, Column, Integer, DateTime, Date, String
from sqlalchemy.dialects.postgresql import JSONB
from geoalchemy2 import Geography

metadata = MetaData()
# items = Table('items', metadata,
#     Column('scraping_session_uuid', String()),
#     Column('created_at', DateTime()),
#     Column('attribute', String()),
#     Column('data_date', Date()),
#     Column('data_value', Integer()),
# )

places_table = Table('places', metadata,
    Column('place_id', String()),
    Column('data', JSONB),
    Column('source_url', String()),
    # Column('created_at', DateTime()),
)

locations_table = Table('locations', metadata,
    Column('address_alias', String()),
    Column('address_line_1', String()),
    Column('city', String()),
    Column('state', String()),
    Column('zip', String()),
    Column('lat_lng', Geography()),
    Column('property_type', String()),
    Column('property_name', String()),
    Column('place_id', String()),
)