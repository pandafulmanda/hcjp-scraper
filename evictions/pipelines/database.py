from databases import Database
from scrapy.exceptions import NotConfigured
from sqlalchemy import create_engine

class DatabasePipeline(object):
    def __init__(self, database_url):
        self.database_url = database_url
        self.engine = create_engine(self.database_url)

    @classmethod
    def from_crawler(cls, crawler):
        database_settings = crawler.settings.getdict("DATABASE_SETTINGS")
        if not database_settings:
            raise NotConfigured
        database_url = database_settings['database_url']
        return cls(database_url)

    def open_spider(self, spider):
        self.conn = self.engine.connect()

    async def process_item(self, item, spider):
        # if (item.get('type') is 'row'):
        #     self.conn.execute(items.insert(), item.get('items'))
        return item

    def close_spider(self, spider):
        self.conn.close()