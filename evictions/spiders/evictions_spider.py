# -*- coding: utf-8 -*-
import scrapy
from evictions.items import EvictionsItem
from evictions.utils import COURTS, create_chunks
from datetime import date, timedelta

from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY


def get_first_of_month(date_arg):
    return date_arg + relativedelta(day=1)


def get_last_of_month(date_arg):
    return date_arg + relativedelta(day=31)


def generate_month_ranges(from_date, to_date):
    range_start = get_first_of_month(from_date)
    range_end = get_last_of_month(to_date)

    ranges = list(rrule(MONTHLY, dtstart=range_start,
                        until=range_end, bymonthday=(1, -1,)))
    return list(create_chunks(ranges, 2))


class EvictionsSpider(scrapy.Spider):
    name = "evictions"

    def start_requests(self):
        scrape_date = date.today()

        extract_type = getattr(self, 'extract_type', 'CV')
        court = getattr(self, 'court', None)
        request_type = getattr(self, 'request_type', None)
        case_type = getattr(self, 'case_type', 'EV')
        from_date = date.fromisoformat(
            getattr(self,  'from_date', scrape_date.strftime("%Y-%m-%d")))
        to_date = date.fromisoformat(
            getattr(self, 'to_date', scrape_date.strftime("%Y-%m-%d")))
        data_format = getattr(self, 'data_format', 'tab')
        url = 'http://jpwebsite.harriscountytx.gov/PublicExtracts/GetExtractData?extractCaseType={extract_type}&extract={request_type}&court={court}&casetype={case_type}&format={data_format}&fdate={fdate}&tdate={tdate}'

        if court is not None:
            courts = [court]
        else:
            courts = COURTS

        date_ranges = generate_month_ranges(from_date, to_date)

        if request_type is not None:
            request_types = [request_type]
        else:
            request_types = [1, 3]

        for court in courts:
            for request_type in request_types:
                for [fdate, tdate] in date_ranges:
                    formatted_url = url.format(extract_type=extract_type, request_type=request_type, court=court,
                                               case_type=case_type, data_format=data_format, fdate=fdate.strftime("%m%%2F%d%%2F%Y"), tdate=tdate.strftime("%m%%2F%d%%2F%Y"))
                    yield scrapy.Request(formatted_url, callback=self.parse, meta={'from_date': fdate, 'case_type': case_type, 'request_type': request_type, 'court': court})

    def parse(self, response):
        file_url = response.url
        item = EvictionsItem()
        item['file_urls'] = [file_url]
        item['from_date'] = response.meta.get('from_date')
        item['case_type'] = response.meta.get('case_type')
        item['request_type'] = response.meta.get('request_type')
        item['court'] = response.meta.get('court')
        item['precinct'] = COURTS[response.meta.get('court')]['precinct']
        item['place'] = COURTS[response.meta.get('court')]['place']
        item['judge'] = COURTS[response.meta.get('court')]['judge']
        item['request'] = response.request
        item['response'] = response
        yield item
