# -*- coding: utf-8 -*-
import scrapy
from evictions.items import DocketsItem
from evictions.utils import COURTS, create_chunks
from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY
import re


def get_week_from_day(date_arg):
    return date_arg + timedelta(days=6)


class DocketsSpider(scrapy.Spider):
    name = "dockets"
    custom_settings = {
        'ITEM_PIPELINES': {
            'evictions.pipelines.DocketToAirtable': 400
        }
    }

    def start_requests(self):
        scrape_date = date.today()
        court_list = [str(d['precinct'])+str(d['place'])
                      for d in list(COURTS.values())]
        url = 'https://jpwebsite.harriscountytx.gov/WebDockets/GetDocketSummary?court={court}&fdate={fdate}&tdate={tdate}'

        court = getattr(self, 'court', None)
        fdate = date.fromisoformat(
            getattr(self,  'fdate', scrape_date.strftime("%Y-%m-%d")))
        tdate = date.fromisoformat(
            getattr(self,  'tdate', get_week_from_day(fdate).strftime("%Y-%m-%d")))

        court_list = ["{precinct}{place}".format(precinct=d['precinct'], place=d[
            'place']) for d in list(COURTS.values())]

        if court is not None:
            courts = [court]
        else:
            courts = court_list

        for court in courts:
            formatted_url = url.format(court=court, fdate=fdate.strftime(
                "%m%%2F%d%%2F%Y"), tdate=tdate.strftime("%m%%2F%d%%2F%Y"))
            yield scrapy.Request(formatted_url, callback=self.parse, meta={'from_date': fdate, 'to_date': tdate, 'court': court})

    def parse(self, response):
        dockets = []
        item = DocketsItem()
        item['court'] = response.meta.get('court')
        current_cases = []
        current_date = None
        current_time = None
        current_link = None
        elements = response.css(
            '.contentWithNavBar .docketDateHeader, .contentWithNavBar .docketTimeHeader, .record')

        for element in elements:
            BASE_LINK = 'https://jpwebsite.harriscountytx.gov/WebDockets/'

            element_type = element.xpath('@class').get()
            if element_type == 'docketDateHeader':
                current_date = re.findall(
                    "[0-9]{2}\/[0-9]{2}\/[0-9]{4}", element.css('a::attr(href)').extract_first())[0]

            if element_type == 'docketTimeHeader':
                current_time = re.findall(
                    "[0-9]{2}:[0-9]{2}[A|P]M",  element.css('a::text').extract_first())[0]
                current_link = BASE_LINK + element.css('a::attr(href)').extract_first()

            if element_type == 'record':
                case_type = element.xpath(
                    ".//span[contains(concat(' ', @class, ' '), 'eventDescription')]").css('::text').extract_first()
                num_case = element.xpath(
                    ".//span[contains(concat(' ', @class, ' '), 'numberOfCases')]").css('::text').extract_first()
                if case_type and num_case:
                    current_cases.append(
                        {'type_name': case_type, 'num_cases': int(num_case)})

                total_header = element.xpath(
                    ".//span[contains(concat(' ', @class, ' '), 'totalHeader')]")

                if(total_header):
                    item['docket_date'] = current_date
                    item['docket_time'] = current_time
                    item['docket_link'] = current_link
                    item['docket_cases'] = current_cases
                    dockets.append(item)

                    item = DocketsItem()
                    item['court'] = response.meta.get('court')
                    current_cases = []

        yield {
            'dockets': dockets,
            'court': response.meta.get('court'),
            'from_date': response.meta.get('from_date'),
            'to_date': response.meta.get('to_date')
        }
