# -*- coding: utf-8 -*-
import scrapy
from evictions.items import DocketsItem
from evictions.utils import COURTS, create_chunks
from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY
import re

def get_week_from_day(date_arg):
    return date_arg + timedelta(days=6)

class CasesOnDocketSpider(scrapy.Spider):
    name = "cases_on_docket"
    custom_settings = {
        'ITEM_PIPELINES': {
            # 'evictions.pipelines.DocketToAirtable': 400
        }
    }

    def start_requests(self):
        scrape_date = date.today()
        court_list = [str(d['precinct'])+str(d['place'])
                      for d in list(COURTS.values())]

        url = 'https://jpwebsite.harriscountytx.gov/WebDockets/GetDocketDetail?date={court_date}&court={court}&media=screen'


        court = getattr(self, 'court', None)
        fdate = date.fromisoformat(
            getattr(self,  'fdate', scrape_date.strftime("%Y-%m-%d")))
        tdate = date.fromisoformat(
            getattr(self,  'tdate', get_week_from_day(fdate).strftime("%Y-%m-%d")))

        court_list = ["{precinct}{place}".format(precinct=d['precinct'], place=d[
            'place']) for d in list(COURTS.values())]

        if court is not None:
            courts = [court]
        else:
            courts = court_list

        for court in courts:
            court_date = fdate
            while court_date <= tdate:
                formatted_url = url.format(court=court, court_date=court_date.strftime("%m%%2F%d%%2F%Y"))
                yield scrapy.Request(formatted_url, callback=self.parse_cases, meta={'court_date': court_date, 'court': court})
                court_date = court_date + timedelta(days=1)

    def parse_cases(self, response):
        record_elements = response.css('.docketRecord')
        case_parts = ['case_id', 'case_style', 'case_type']

        for record_el in record_elements:
            record_classes = record_el.xpath('@class').get()
            case_detail_strings = record_el.xpath(".//div[contains(@class, 'caseDetails')]").css('::text').extract()

            case_details = dict(zip(
                case_parts, [
                    detail.strip().replace('\n', '')
                    for detail in [
                        case_detail_strings[1]] + case_detail_strings[-2:]
                    ]
                ))
            case_details['court'] = 'Precinct {0} Place {1}'.format(*list(response.meta.get('court')))
            case_details['court_date'] = response.meta.get('court_date')
            case_details['court_time'] = datetime.strptime(record_classes.split(' ')[2], '%I%M%p').strftime('%I:%M %p')

            yield case_details


        

