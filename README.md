# About the data

[Source Form](https://jpwebsite.harriscountytx.gov/PublicExtracts/search.jsp)

The following urls are where the form submits to.  When it does so, a tab delimited file is downloaded.

`https://jpwebsite.harriscountytx.gov/PublicExtracts/GetExtractData?extractCaseType=CV&extract=1&court=305&casetype=EV&format=tab&fdate=04%2F01%2F2020&tdate=04%2F30%2F2020`

`https://jpwebsite.harriscountytx.gov/PublicExtracts/GetExtractData?extractCaseType=CV&extract=3&court=305&casetype=EV&format=tab&fdate=04%2F01%2F2020&tdate=04%2F30%2F2020`

## Note on the query parameters

| Form Label | Options | Parameter Name | Parameter Options | Our target(s) |
|------------|---------|-----------------|---------------|------------|
| Extract Type | Criminal, Civil | extractCaseType | CR, VR | CV |
| Extract to Request | Cases Entered, Hearings Set, Judgements Entered (different for criminal) | extract | 1, 2, 3 | 1 & 3 first |
| Court | See Below | court | See Below | all |
| Case Type(s) to Include | Eviction, many others | casetype | EV, others | EV |
| Data Format | Tab-Delimited Text, CSV, XML | format | tab, csv, xml | tab |
| From Date | | fdate | MM/DD/YYYY | start of month |
| To Date | | tdate | MM/DD/YYYY | end of month | 

### Court Options

Just throwing this in here.  Can be any format when we code.
```
{
    "305" : "Precinct 1, Place 1",
    "310" : "Precinct 1, Place 2",
    "315" : "Precinct 2, Place 1",
    "320" : "Precinct 2, Place 2",
    "325" : "Precinct 3, Place 1",
    "330" : "Precinct 3, Place 2",
    "335" : "Precinct 4, Place 1",
    "340" : "Precinct 4, Place 2",
    "345" : "Precinct 5, Place 1",
    "350" : "Precinct 5, Place 2",
    "355" : "Precinct 6, Place 1",
    "360" : "Precinct 6, Place 2",
    "365" : "Precinct 7, Place 1",
    "370" : "Precinct 7, Place 2",
    "375" : "Precinct 8, Place 1",
    "380" : "Precinct 8, Place 2"
}
```

# Scope

After some thinking yesterday evening, I realized I was rushing a bit when talking about options.  I started right on Scrapy, but I did not talk about alternates.

In a lot of ways, because this scraping job is so simple, this framework is overkill.  This task could be scripted using bash and curl, wget, or httpie, etc.  Scrapy gives us versatility to scale easily though.  I think their design lends itself to a pretty frictionless experience.  If you have an interest in another tool, as long as we can maintain it, please feel free to explore!

# Design

It would be nice for whatever gets built to be parameterized by:

* Month
    * month would be default, could provide a more precise range if desired
* Court
* Extract to Request

It would be nice if it has nice defaults:

* Month -- defaults to available range for current month (i.e. May 1 - May 13)
* Court -- defaults to a list of all
* Extract to Request -- defaults to a list of all

This let's us, in the automated -- the most common use case in the future -- call of this be able to pass in no options.

And, in the case that we find missing data, in debugging, in needing more precision, we are able to by providing values for options.

# Setting up

Clone, pipenv shell, pipenv install, etc.

I've taken the example [Spider with arguments](https://docs.scrapy.org/en/latest/intro/tutorial.html#using-spider-arguments) and put it in our [`./evictions/spiders/example.py`](./evictions/spiders/example.py).

You should be able to run

`scrapy crawl quotes` and `scrapy crawl quotes -a tag=humor`

.  If you see quotes being piped to your stdin, your project is all set up locally and you're ready to scrape on!

As a refresher/to build familiarity, I find their [tutorial](https://docs.scrapy.org/en/latest/intro/tutorial.html) to be pretty good -- straightforward and useful.

Feel free to remove the file after use :).

# Roadmap

Rough idea, please adapt as needed :)

* Scrape/download data by backfilling
* Set up a scheduler to run scraping for upcoming data
* Some initial data analysis to understand what we got
* Design database schema
* Transform from files to database
* Set up pipeline from scraper directly into database

# Future

One day, if we find ourselves continuing to make scrapers, it may make sense for us to set up a [CookieCutter](https://cookiecutter.readthedocs.io/en/1.7.2/#) for our preferred configuation.